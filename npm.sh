curl -sSL https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/bin \
    && npm cache clean -f \
    && npm install -g n \
    && n stable \
    && npm install -g grunt-cli \
    && gem update \
    && rm -rf /root/.npm /root/.composer /tmp/* /var/lib/apt/lists/* \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get -y install yarn
