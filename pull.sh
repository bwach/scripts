#!/bin/bash

git config --global user.email "bartlomiej.wach@websitebutler.de"
git config --global user.name "Bartlomiej Wach"

for i in /var/www/html/vendor/websitebutler/*; do
  if [ -d "${i}" ] ; then
    cd $i
    git pull
  fi
done

cd /var/www/html/vendor/websitebutler/assembler
git checkout 1.6.1
cd /var/www/html

git pull

sudo rm var/* -R
chmod 777 var

grunt cache
