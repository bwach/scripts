#!/bin/bash

git config --global user.email "bartlomiej.wach@websitebutler.de"
git config --global user.name "Bartlomiej Wach"

for i in /var/www/html/vendor/websitebutler/*; do
  if [ -d "${i}" ] ; then
    cd $i
    pwd
    git status
    #git status | grep branch
  fi
done

