apt-get -y install php5.6-dev php-pear php5.6-mysqlnd php5-mcrypt php5.6-curl php5.6-intl
apt-get -y install php5.6-gettext php5.6-json php5.6-geoip php5.6-apcu 
apt-get -y install php5.6-dev php5.6-gd php5.6-imagick php5-xdebug php5.6-xhprof php5.6-xmlrpc
apt-get -y install imagemagick libmagickwand-dev openssh-client curl software-properties-common gettext zip mysql-server mysql-client apt-transport-https python python3 perl php5-memcached memcached libmcrypt-dev mongodb-server mongodb libxslt-dev zlib1g-dev libicu-dev libxml2-dev
apt-get -y install vim ruby ruby-dev ruby-sass
apt-get -y install unoconv pandoc tesseract-ocr

# PHP extensions
pecl install imagick \
    && echo extension=imagick.so >> /usr/local/etc/php/conf.d/imagick.ini \
    && docker-php5.6-ext-install gd \
    && docker-php5.6-ext-install bcmath \
    && pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini \
